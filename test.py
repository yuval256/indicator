import numpy as np
import pytest
from pandas import Series
from pandas.testing import assert_series_equal

from indicator import rsi


def test_rsi():
    data = Series({
        1577833200000: 1,
        1577833260000: 0,
        1577833320000: 1,
        1577833380000: 2,
    })

    result = rsi(data, periods=2)
    expected = Series({
        1577833200000: np.NaN,
        1577833260000: np.NaN,
        1577833320000: np.NaN,
        1577833380000: 75.0,
    })

    assert_series_equal(result, expected)

    with pytest.raises(AssertionError):
        rsi(data, periods=-1)
