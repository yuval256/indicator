# Indicator

Simple service that reads OHLCV data and calculates the Relative Strength Index (RSI)

Install requirements:
```
$ pip install -r requirements.txt
```
