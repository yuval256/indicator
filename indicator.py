from typing import Union

import pandas as pd
from pandas import DataFrame, Series, Timedelta, Timestamp


def load_json_data(path: str, datetime_index_col: str = '_id') -> DataFrame:
    """Load time series data to DataFrame

    Args:
        path: File path to .json
        datetime_index_col: the name of the key to use as the DatetimeIndex

    Returns:
        DataFrame with DatetimeIndex
    """
    data = pd.read_json(path, convert_dates=[datetime_index_col])
    data.set_index(datetime_index_col, inplace=True)
    return data


def resample_ohlcv_data(data: DataFrame,
                        start_time: Union[str, Timestamp] = None,
                        end_time: Union[str, Timestamp] = None,
                        timeframe: Union[str, Timedelta] = None,
                        ) -> DataFrame:
    """Samples data according to the given arguments,
       and returns a new DataFrame

    Args:
        data: OHLCV data
        start_time: slice the data from time
        end_time: slice the data to time
        timeframe: time period used to aggregate together candles

    Returns:
        DataFrame with resampled data
    """
    sample = data
    if start_time or end_time:
        sample = sample.loc[start_time: end_time]
    if timeframe:
        sample = sample.resample(timeframe).agg({
            'open': 'first',
            'close': 'last',
            'high': 'max',
            'low': 'min',
            'volume': 'sum',
            'close_time': 'last',
            'quote_asset_volume': 'sum',
            'number_of_trades': 'sum',
        })
    return sample


def rsi(data: Series, periods: int = 14) -> Series:
    """Calculate Relative Strength Index (RSI)

    See: https://www.investopedia.com/terms/r/rsi.asp

    Args:
        data: Time series with prices
        period: Candle period for the RSI calculation

    Returns:
        Time series with RSI values
    """

    delta = data.diff(1)
    assert 1 <= periods <= len(delta)

    gain, loss = delta, delta.copy()
    gain[gain < 0] = 0
    loss[loss > 0] = 0
    loss = loss.abs()

    previous_average_gain = gain.rolling(periods).mean().shift(1)
    previous_average_loss = loss.rolling(periods).mean().shift(1)

    rs = ((previous_average_gain * (periods - 1) + gain) /
          (previous_average_loss * (periods - 1) + loss))
    rsi = 100 - (100 / (1 + rs))
    return rsi
